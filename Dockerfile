FROM golang:alpine AS build-env
RUN mkdir /go/src/app
ADD main.go /go/src/app/
WORKDIR /go/src/app
#added to test
RUN apk update
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o app .

FROM scratch
WORKDIR /app
COPY --from=build-env /go/src/app/app .
ENTRYPOINT [ "./app" ]
